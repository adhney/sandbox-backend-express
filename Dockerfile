# Use the official Node.js LTS (Long Term Support) image as the base image
FROM node:18

# Set the working directory inside the container
WORKDIR /app

# Copy the package.json and package-lock.json to the container
COPY package*.json ./

# Install app dependencies
RUN yarn

# Copy the rest of the application code to the container
COPY . .

# Expose the port that your Express app is listening on
EXPOSE 3000

# Command to start the app
CMD [ "yarn", "debug" ]
